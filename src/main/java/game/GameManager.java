package game;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import gameObjects.wearable.*;

public class GameManager {
	public static void main(String[] args) {
		
		System.out.println("~~WELCOME TO LEGEND OF ZELDA!~~");
		MongoClient mc = MongoClients.create("mongodb://localhost:27017");
		MongoDatabase md = mc.getDatabase("zelda");
		MongoCollection<Document> mcol = md.getCollection("GameData");
		
		
		Player p;
		Document playerDocInDB = mcol.find().first();
		if(playerDocInDB != null) {
			p = new Player(playerDocInDB);
		}else {
			p = new Player();
			Helmet h = new Helmet("Shadow helmet" , 10 , 2);
			p.addWearable(h);				
		}
		p.damage(50);		
		
		mcol.insertOne(p.getBsonDocument());
	}
}
