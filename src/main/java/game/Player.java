package game;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import gameObjects.wearable.*;

public class Player {
	int health;	
	ArrayList<Wearable> wearables;
	
	public Player() {
		wearables = new ArrayList<Wearable>();
		health = 100;		
	}
	
	public Player(Document doc) {
		health = doc.getInteger("health", -1);
		wearables = new ArrayList<Wearable>();
		
		List<Document> docs=
				doc.getList("wearables", Document.class);
		
		for(int i =0;i<docs.size();i++) {
			Helmet h = new Helmet(docs.get(i));
			wearables.add(h);
		}
		
	}
	
	public int getHealth() {
		return health;
	}
	
	public int getArmor() {
		int armor = 0 ;
		for(int i = 0 ; i < wearables.size() ; i++) {
			Wearable w = wearables.get(i);
			if(w instanceof Armor)
			{
				armor += ((Armor)w).getArmorAmount();
			}
		}
		return armor;
	}
	 

	public void addWearable(Wearable w) {
		if(!wearables.contains(w)) {
			wearables.add(w);			 
		}				
	}
	
	public void damage(int damageAmount) {
		int pureDamage = damageAmount - getArmor();
		if(pureDamage > 0) {			
			health -= pureDamage;
		}
		
		if(health <= 0) {
			health = 0;
		}
	}
	
	public Document getBsonDocument() {
		Document doc = new Document();
		doc.append("health", health);
		
		ArrayList<Document> wearableDocs =
				new ArrayList<Document>();
		
		for(int i = 0 ; i < wearables.size();i++) {
			wearableDocs.add(wearables.get(i).getBsonDocument());
		}
		
		doc.append("wearables", wearableDocs);
		
		return doc;
	}
}

 