package gameObjects.wearable;

import org.bson.Document;

public abstract class Armor extends Wearable {
	private int armorAmount;
	
	public Armor(String name, int armorAmount, int weight) {
		super(name,weight);
		this.armorAmount = armorAmount;		
		type= "armor";
	}
	
	public Armor(Document doc) {
		super(doc);
		armorAmount = doc.getInteger("armorAmount",0);
	}
	
	public int getArmorAmount() {
		return armorAmount;
	}
	
	@Override
	public Document getBsonDocument() {
		Document doc = super.getBsonDocument();
		doc.append("armorAmount", armorAmount);
		return doc;				
	}
 
}
