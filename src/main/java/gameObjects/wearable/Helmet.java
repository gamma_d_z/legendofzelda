package gameObjects.wearable;

import org.bson.Document;

public class Helmet extends Armor {
	public Helmet(String name,int armorAmount,int weight) {
		super(name,armorAmount,weight);
		type= "helmet";
	}
	
	public Helmet(Document doc) {
		super(doc);
	}
	
	@Override
	public Document getBsonDocument() {		
		return super.getBsonDocument();
	}
}
