package gameObjects.wearable;

import org.bson.Document;

public abstract class Wearable {
	
	private final String NAME = "name"; 
	
	String name;
	int weight;
	int duration;
	boolean isAlive;
	protected String type;
	
	public Wearable(String name, int weight) {
		super();
		this.name = name;
		this.weight = weight;
		duration = 100;
		isAlive = true;		
		type= "no_type";
		
	}
	
	public Wearable(Document doc) {
		super();
		this.name = doc.getString(NAME);
		this.weight = doc.getInteger("weight", 0);
		this.isAlive = doc.getBoolean("isAlive");
		this.duration = doc.getInteger("duration", 0);		
		this.type = doc.getString("type");		
	} 
	
	public Document getBsonDocument() {
		Document doc = new Document();
		doc.append(NAME, name);
		doc.append("weight", weight);
		doc.append("duration", duration);
		doc.append("isAlive", isAlive);
		doc.append("type", type);	
		return doc;
	}
	
}
